import React, { Component } from 'react';

class GheItem extends Component {
    // state={
    //     isBooking:false,
    // }
    // handleCLick=(item)=>{
    //     // this.setState({
    //     //     isBooking:!this.state.isBooking,
    //     // })
    //     this.props.add(item);
        
    // }

    renderClass=(TrangThai)=>{
        if(TrangThai){
            return "bg-danger text-white w-50 p-3"
        }else {
            // if(this.state.isBooking){

            //     return "bg-success text-white w-50 p-3"
            // }else{
               
            //     return "bg-secondary text-white w-50 p-3 "
            // }
            const isghe=this.props.listGhe.findIndex(item=> item.SoGhe === this.props.item.SoGhe);

            if(isghe !== -1){
                return "bg-success text-white w-50 p-3"
            }else{
                return "bg-secondary text-white w-50 p-3 "
            }
        }
    };
    render() {
        const {TrangThai,SoGhe} = this.props.item;
        return (
              <div className="col-3 my-1">
                    <button 
                        style={{border:"none",}}
                        // className={TrangThai?"bg-danger text-white w-50 p-3":this.state.isBooking?"bg-success text-white w-50 p-3":"bg-secondary text-white w-50 p-3 "}
                        className={this.renderClass(TrangThai)}
                      
                        disabled={TrangThai?true:false}
                       onClick={()=>this.props.add(this.props.item)}
                       
                    >
                        {SoGhe}
                    </button>
              </div>
        );
    }
    
}

export default GheItem;

