import React, { Component } from 'react';

class DanhSachGheDangDat extends Component {

    render() {
        return (
            <div>
                <h5 className="text-warning">DANH SÁCH GHẾ ĐÃ ĐẶT  {this.props.listGhe.length}</h5>
                    {this.props.listGhe.map(item=>{
                         return (
                             <div key={item.SoGhe} className="d-flex justify-content-center align-items-center" >
                               
                                 <p className=" text-center"> Ghế : {item.TenGhe}  $ {item.Gia} </p>
                                 <p onClick={()=>{this.props.delete(item.SoGhe)}} className="px-2 text-danger" href="#">[Hủy]</p>
                                
                             </div>
                    );
              })}
            </div>
        );
    }
}

export default DanhSachGheDangDat;