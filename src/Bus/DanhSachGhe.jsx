import React, { Component } from 'react';
import GheItem from './GheItem';

class DanhSachGhe extends Component {
    

    render() {
        return (
            <div>
                <div className="row">
                    {
                        this.props.listBus.map(item=>(
                            <GheItem key={item.SoGhe} item={item} add={this.props.addList} listGhe={this.props.listGhe}/>
                        ))
                    }
                </div>
            </div>
        );
    }
}

export default DanhSachGhe;