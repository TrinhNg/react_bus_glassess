import React, { Component } from "react";
import GlassetItem from "./glassetItem";

class GlassesList extends Component {
  renderGlass = () => {
    return this.props.glassesList.map((item) => {
      return (
        <div style={{ margin: "10px 0" }}>
          <GlassetItem
            key={item.id}
            item={item}
            addGlasses={this.props.addGlasses}
          />
        </div>
      );
    });
  };
  render() {
    return (
      <div style={{ backgroundColor: "white", display: "flex" }}>
        {this.renderGlass()}
      </div>
    );
  }
}

export default GlassesList;
