import React, { Component } from "react";
import GlassesList from "./glassesList";
import "./home.css";

class Home extends Component {
  arrProduct = [
    {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: "./assets/img/glassesImage/v1.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
    },
    {
      id: 2,
      price: 50,
      name: "GUCCI G8759H",
      url: "./assets/img/glassesImage/v2.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
    },
    {
      id: 3,
      price: 30,
      name: "DIOR D6700HQ",
      url: "./assets/img/glassesImage/v3.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
    },
    {
      id: 4,
      price: 30,
      name: "DIOR D6005U",
      url: "./assets/img/glassesImage/v4.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
    },
    {
      id: 5,
      price: 30,
      name: "PRADA P8750",
      url: "./assets/img/glassesImage/v5.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
    },
    {
      id: 6,
      price: 30,
      name: "PRADA P9700",
      url: "./assets/img/glassesImage/v6.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
    },
    {
      id: 7,
      price: 30,
      name: "FENDI F8750",
      url: "./assets/img/glassesImage/v7.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
    },
    {
      id: 8,
      price: 30,
      name: "FENDI F8500",
      url: "./assets/img/glassesImage/v8.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
    },
    {
      id: 9,
      price: 30,
      name: "FENDI F4300",
      url: "./assets/img/glassesImage/v9.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
    },
  ];

  state = {
    glassesList: this.arrProduct,
    glassesItem: {}, // ko phai là mang
  };

  addGlasses = (item) => {
    let clone = { ...this.state.glassesItem };
    if (clone.id === item.id) {
      clone = {};
    } else {
      clone = item;
    }
    this.setState({
      glassesItem: clone,
    });
  };

  render() {
    return (
      <div
        className="container"
        style={{
          backgroundImage: "url(../assets/img/glassesImage/background.jpg)",
        }}
      >
        <h1 className="text-center">TRY GLASSES APP ONLINE</h1>
        <div
          className={this.state.glassesItem.url ? "img" : "img opacity"}
          style={{
            backgroundImage: "url(./assets/img/glassesImage/model.jpg)",
          }}
        >
          <img
            src={this.state.glassesItem.url}
            className="img__change"
            alt=""
          />
          <div className="desc">
            <h3>{this.state.glassesItem.name}</h3>
            <p>{this.state.glassesItem.desc}</p>
          </div>
        </div>
        <GlassesList
          addGlasses={this.addGlasses}
          glassesList={this.state.glassesList}
          style={{ width: 100 }}
        />
      </div>
    );
  }
}

export default Home;
