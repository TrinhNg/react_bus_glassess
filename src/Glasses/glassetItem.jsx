import React, { Component } from "react";
import "./glassesItem.css";

class GlassetItem extends Component {
  render() {
    const { url } = this.props.item;
    return (
      <div
        onClick={() => {
          this.props.addGlasses(this.props.item);
        }}
        style={{ margin: 10, padding: 5, border: "1px solid #eee" }}
      >
        <img style={{ width: 80 }} src={url} alt="" />
      </div>
    );
  }
}

export default GlassetItem;
